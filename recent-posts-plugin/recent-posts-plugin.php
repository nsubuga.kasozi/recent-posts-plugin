<?php
/**
 * recent-posts Plugin is the simplest WordPress plugin for beginner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package recent-posts Plugin
 * @author recent-posts
 * @license GPL-2.0+
 * @link https://recent-posts.com/tag/wordpress-beginner/
 * @copyright 2017 Kasozi, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: recent-posts Plugin
 *            Plugin URI: https://recent-posts.com/tag/wordpress-beginner/
 *            Description: recent-posts Plugin
 *            Version: 3.0
 *            Author: Kasozi
 *            Author URI: https://recent-posts.com/
 *            Text Domain: recent-posts
 *            Contributors: recent-posts
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// @COMMENT: This header could have just been the last part after the '@copyright' line . Something like this:
// *            Plugin Name: Recent Posts Plugin
// *            Plugin URI: https://kanzucode.com
// *            Description: Display recent posts
// *            Version: 3.0
// *            Author: Kasozi
// *            Author URI: https://kanzucode.com
// *            Text Domain: recent-posts
// *            Contributors: recent-posts
// *            License: GPL-2.0+
// *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

// @COMMENT: WP generally uses underscores when naming so this would be Recent_Posts. Adding 'Plugin' to the class name isn't necessary
class RecentPostsPlugin {

	// @COMMENT: WordPress generally uses snake_case in naming. See https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/
	// @COMMENT: Update these to use snake_case
	public $Settings_Options_Group = 'recent-posts-options';
	public $Settings_Options_Name = 'recent-posts-options';
	public $default_number_of_posts = 3;

	public function __construct() {
		// @TODO Something more descriptive of what's happening would be better
		WriteToLog( 'Settings_Options_Group:' . $this->Settings_Options_Group );
		WriteToLog( 'Settings_Options_Name:' . $this->Settings_Options_Name );
	}


	public function Register() {
		add_action( 'admin_menu', array( $this, 'plugin_admin_add_page' ) );
		WriteToLog( 'admin_menu: Added Menu' );
		add_action( 'admin_init', array( $this, 'RegisterSettings' ) );
		WriteToLog( 'admin_init: Intialized Init' );

	}

	public function save_updated_setting_callback() {
		$SettingGroup = $this->Settings_Options_Group;
		WriteToLog( 'save_updated_setting_callback was called' );
		if ( isset( $_POST[ $SettingGroup ] ) ) {
			$this->SaveSettingValue();
		}
		$redirect_url = 'Location: ' . admin_url( 'admin.php' ) . '?page=recent-posts-options';
		header( $redirect_url ); /*
		Redirect browser */
		// @TODO WordPress has an inbuilt function to help with this,https://developer.wordpress.org/reference/functions/wp_redirect/
		WriteToLog( "Redirected to $redirect_url" );
		exit();
	}

	public function plugin_admin_add_page() {
		$name = $this->Settings_Options_Group;
		add_menu_page( $name, $name, 'manage_options', $name, array( $this, 'GetOptionsPageHtml' ) );
		WriteToLog( 'add_options_page: Added Menu' );
	}

	public function RegisterSettings() {
		register_setting( $this->Settings_Options_Group, $this->Settings_Options_Name );
		WriteToLog( 'Register Settings: Done' );
		add_settings_section( 'plugin_main', 'Main Settings', array( $this, 'plugin_section_text' ), $this->Settings_Options_Name );
		WriteToLog( 'add_settings_section: plugin_main' );
		add_settings_field( 'plugin_text_string', 'Number of Posts To Display', array( $this, 'plugin_setting_string' ), $this->Settings_Options_Name, 'plugin_main' );
		WriteToLog( 'add_settings_field: plugin_text_string' );
		add_action( 'admin_post_custom_action_hook', array( $this, 'save_updated_setting_callback' ) );
	}

	public function plugin_section_text() {
		// echo '<p>Main description of this section here.</p>'; @TODO Don't leave commented out code in the final code
		WriteToLog( 'plugin_section_text' );
	}

	public function plugin_setting_string() {
		$options = get_option( $this->Settings_Options_Group );
		WriteToLog( 'options:' . $options );
		$SettingName = $this->Settings_Options_Name;
		$SettingGroup = $this->Settings_Options_Group;
		echo "<input id='plugin_text_string' name='$SettingGroup' size='40' type='text' value='" . $this->GetNumberOfPostsPerPage() . "' />";
		WriteToLog( 'plugin_setting_string' );
	}

	// build html form
	public function GetOptionsPageHtml() {
		echo('<div class="wrap">');
		echo('<h1>Recent Posts Settings</h1>');

		$SettingGroup = $this->Settings_Options_Group;

		echo('<form method="post" action="' . admin_url( 'admin-post.php' ) . '?page=recent-posts-options"> ');
		echo "<input type='hidden' name='action' value='custom_action_hook' />";
		$this->plugin_setting_string();
		submit_button();
		echo('</form>');
		echo('<h1>Your Recent Posts</h1>');
		$this->GetRecentPosts();
		echo('</div>');
		WriteToLog( 'GetOptionsPageHtml' );
	}

	// get the most recent posts and echo them out
	public function GetRecentPosts() {
		$numberOfPosts = $this->GetNumberOfPostsPerPage();
		WriteToLog( "numberOfPosts = $numberOfPosts" );

		$args = array(
		   'numberposts' => $numberOfPosts,
		   'offset' => 0,
		   'category' => 0,
		   'orderby' => 'post_date',
		   'order' => 'DESC',
		   'include' => '',
		   'exclude' => '',
		   'meta_key' => '',
		   'meta_value' => '',
		   'post_type' => 'post',
		   'post_status' => 'draft, publish, future, pending, private',
		   'suppress_filters' => true,
		);

		$recent_posts = wp_get_recent_posts( $args );

		foreach ( $recent_posts as $recent ) {
			echo '<li><a href="' . get_permalink( $recent['ID'] ) . '">' . $recent['post_title'] . '</a> </li> ';
		}

		wp_reset_query();
		WriteToLog( 'GetRecentPosts' );
	}

	public function SaveSettingValue() {
		// get whatever he has input
		$SettingGroup = $this->Settings_Options_Group;
		// @TODO Look at WordPress security guidelines, particularly, securing user input https://developer.wordpress.org/plugins/security/securing-input/
		$new_value_supplied = $_POST[ $SettingGroup ];

		// validate
		if ( ! is_numeric( $new_value_supplied ) ) {
			echo("<h2>Setting of [$new_value_supplied] is not an INT</h2>");
			return;
		}

		// update
		echo("<h1>Setting of $new_value_supplied was Saved</h1>");
		$numberOfPostsPerPage = $new_value_supplied;
		$this->UpdateNumberOfPostsPerPage( $numberOfPostsPerPage );
	}

	// update setting value
	// @TODO All these methods should be named following snake_case
	public function UpdateNumberOfPostsPerPage( int $numberOfPosts ) {
		update_option( $this->Settings_Options_Group, $numberOfPosts );
		return true;
	}

	// get the current setting value
	public function GetNumberOfPostsPerPage() {
		return get_option( $this->Settings_Options_Group );
	}

	// insert a post on activation
	public function Activate() {
		$this->UpdateNumberOfPostsPerPage( $this->default_number_of_posts );
		$this->SaveInitialPost();
		flush_rewrite_rules();
		WriteToLog( 'Plugin has been activated Successfully' );
	}

	public function SaveInitialPost() {
		// Create post object
		$my_post = array(
						   'post_title'    => 'Inserted on plugin activation',
						   'post_content'  => 'Recent Posts Plugin Dummy Post Content',
						   'post_status'   => 'publish',
						   'post_author'   => 1,
						   'post_category' => array( 8,39 ), // @TODO How sure are you these categories exist? You'd need to first create them
						   'ID' => 0,// to ensure insertion
					   );

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	public function Deactivate() {
		flush_rewrite_rules();
		WriteToLog( 'Plugin has been De-activated Successfully' );
	}

	public function Uninstall() {
	}
}

 // initialize class
if ( class_exists( 'RecentPostsPlugin' ) ) {
	$recentPostsPlugin = new RecentPostsPlugin();
	$recentPostsPlugin->Register();
}

// writes stuff out to debug.log
function WriteToLog( $log ) {
	if ( is_array( $log ) || is_object( $log ) ) {
		error_log( print_r( $log, true ) );
	} else {
		error_log( $log );
	}
}

// activation
register_activation_hook( __FILE__, array( $recentPostsPlugin, 'activate' ) );

// deactivation
register_deactivation_hook( __FILE__, array( $recentPostsPlugin, 'deactivate' ) );
